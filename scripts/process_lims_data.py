import os
import scipy.stats
import numpy
import matplotlib.pylab as pl


# program path on this machine
#===================================================================
blastneuron_DIR = "/Users/xiaoxiaoliu/work/v3d/vaa3d_tools/blastneuron"
PRUNE_SHORT_BRANCH = blastneuron_DIR + "/bin/prune_short_branch"
PRE_PROCESSING = blastneuron_DIR + "/bin/pre_processing"
NEURON_RETRIEVE = blastneuron_DIR + "/bin/neuron_retrieve"
BATCH_COMPUTE = blastneuron_DIR + "/bin/batch_compute"  # compute faetures
MRMR= "/Users/xiaoxiaoliu/work/src/mrmr_c_src/mrmr"
V3D="/Users/xiaoxiaoliu/work/v3d/v3d_external/bin/vaa3d64.app/Contents/MacOS/vaa3d64"

# data dir
USE_MANUAL = 1
if USE_MANUAL: # use manually edited neuron
    data_DIR = "/Users/xiaoxiaoliu/work/data/lims2/manual"
else:
    data_DIR = "/Users/xiaoxiaoliu/work/data/lims2/auto"


data_linker_file =  data_DIR+'/original/mylinker.ano'
preprocessed_data_linker_file = data_DIR+'/preprocessed/subtypes.ano'
feature_file = data_DIR + '/preprocessed/subtypes_features.nfb'
result_DIR = data_DIR + '/result'


#===================================================================
def prune(inputswc_fn, outputswc_fn):
    cmd = PRUNE_SHORT_BRANCH +  " -i "+inputswc_fn + " -o "+outputswc_fn
    os.system(cmd)
    print cmd
    return

def preprocessing(inputswc_fn, outputswc_fn):
    cmd = PRE_PROCESSING+  " -i "+inputswc_fn + " -o "+outputswc_fn
    os.system(cmd)
    return

def neuronretrive(inputswc_fn, feature_fn, result_fn, retrieve_number, logfile):
    cmd = NEURON_RETRIEVE + " -d " + feature_fn + " -q " +inputswc_fn + " -n "+ \
          str(retrieve_number) +" -o "+result_fn+" -m 1,3" + " >" + logfile
    print cmd
    os.system(cmd)
    return

def featurecomputing(input_linker_fn, feature_fn):
    cmd = BATCH_COMPUTE +  " -i "+input_linker_fn + " -o " + feature_fn
    os.system(cmd)
    print cmd
    return

def genLinkerFile(swcDir, linker_file):
    cmd = V3D + " -x  linker_file_gen  -f linker -i "+ swcDir +" -o "+ linker_file +" -p 1"
    print cmd
    os.system(cmd)
    return

def readDBFeatures(feature_file):
    # TODO: detect nan values
    glf_featureList = []  # each row is a feature vector
    gmi_featureList = []
    with open (feature_file,'r') as  f:
        for fn_line in f: # ignore the SWCFILE=* line
            line_globalFeature = (f.next()).strip()
            glf = map(float,line_globalFeature.split('\t'))
            glf_featureList.append(glf)

            line_GMI = (f.next()).strip()
            gmi = map(float,line_GMI.split('\t'))
            gmi_featureList.append(gmi)

    return  numpy.array(glf_featureList), numpy.array(gmi_featureList)

#===================================================================

def zscore(features):
    zscores=scipy.stats.mstats.zscore(features,0)
    return zscores


def normalizeFeatures(features):
    meanFeatures = numpy.mean(features,0)
    stdFeatures = numpy.std(features, 0)
    normalized = (features - meanFeatures)/stdFeatures

     ## hackish :rearrange data
#    tmp8= normalized[8]
#    tmp9= normalized[9]
#    tmp31= normalized[31]
#
#    normalized=numpy.delete(normalized,8,0)
#    normalized=numpy.delete(normalized,9-1,0)
#    normalized=numpy.delete(normalized,31-2,0)
#
#    normalized=numpy.append(normalized,[tmp31],axis=0)
#    normalized=numpy.append(normalized,[tmp9],axis=0)
#    normalized=numpy.append(normalized,[tmp8],axis=0)
#
    return normalized


def plotFeatureVector(featureArray,fig_title):
    normalized = normalizeFeatures(featureArray)
    pl.figure()
    pl.imshow(normalized,interpolation='none')
    pl.colorbar()
    pl.title(fig_title)
    pl.xlabel('feature ID')
    pl.ylabel('neuron ID')


def plotScoreMatrix(featureArray, fig_title):
    scoreMatrix =[]
    normalized = normalizeFeatures(featureArray)

    for i in range(len(normalized)):
        queryFeature = normalized[i] # each row is a feature vecto
        scores = numpy.exp(-numpy.sum(abs(normalized-queryFeature)**2,1)/100)
        #scores = numpy.sum(abs(normalized-queryFeature)**2,1)
        scoreMatrix.append(scores)

    pl.figure()
    pl.imshow(scoreMatrix,interpolation='none')
    pl.colorbar()
    pl.title(fig_title)
    pl.xlabel('neuron ID')
    pl.ylabel('neuron ID')


def selectFeatures_MRMR(featureArray, threshold=0, number_of_features=5, selection_method='MID'):
    #write out feature array into a csv file, then execute MRMR
    csvfile = data_DIR+"/zscore_glFeatures.csv"

    numpy.savetxt(csvfile, featureArray, delimiter=",")

    # call MRMR
    # cmd = MRMR +  " -i "+ csvfile + " -t "+ threshold + " -n " + number_of_features
    # print cmd
    #os.system(cmd)
    return



#==================================================================================================
def pipeline():

    #genLinkerFile(data_DIR+'/original', data_linker_file )

    if  not os.path.exists(data_DIR+'/pruned'):
        os.mkdir(data_DIR+'/pruned')
    if  not os.path.exists(data_DIR+'/preprocessed'):
        os.mkdir(data_DIR+'/preprocessed')

    with open(data_linker_file,'r') as f:
        for line in f:
            input_swc_fn =  (line.strip()).split('=')[1] #SWCFILE=*
            pruned_swc_fn = data_DIR+'/pruned/'+ os.path.splitext(input_swc_fn)[0]+"_1_pruned.swc"
            #prune(data_DIR+'/original/'+input_swc_fn, pruned_swc_fn)
            preprocessed_swc_fn = data_DIR+'/preprocessed/'+ os.path.splitext(input_swc_fn)[0]+"_2_preprocessed.swc"
           # preprocessing(pruned_swc_fn, preprocessed_swc_fn)

    #genLinkerFile(data_DIR+'/preprocessed',preprocessed_data_linker_file )

    ##batch computing
    featurecomputing(preprocessed_data_linker_file,feature_file)


def analysis():
    glfFeatures, gmiFeatures = readDBFeatures(feature_file)

    csvfile = data_DIR+"/subtypes_glFeatures.csv"
    numpy.savetxt(csvfile, glfFeatures, delimiter=",")

    #normalized = normalizeFeatures(glfFeatures)
    zscores = zscore(glfFeatures)

    selectFeatures_MRMR(zscores)  # save zscores into a csv file

    plotFeatureVector(glfFeatures, "normalized global features")
    plotScoreMatrix(glfFeatures, "Similarity Matrix based on global features")
    plotScoreMatrix(gmiFeatures, "Similarity Matrix based on GMI features")

    return




def main():
    pipeline()  # processing data
    analysis()



if __name__ == "__main__":
      main()
